# docker-forcare-basic
This is a simple docker setup to deploy the forcare suite. Check the docker-compose.yml file for mappings. 

# Instructions:

## Pre-requisites:
1. Have docker installed on local
2. Have an account on www.quay.io with access to 'fc' repo 'www.quay.io/fc'

## Deploying webapps
Copy wars into the ./webapps folder to deploy them in tomcat. There is a script copywars.sh that can be used as an example to copy the war files

## Create forcare/config folder
Create the folder ./forcare-config/config/

## Database
The database schema has to be generated on the first run - use forAdmin tuning advisor to generate this. After initial run the DB should be persisted.

## Run
You need to run the following once per session:
```
docker login quay.io
```

```
docker-compose down && docker-compose up
```
