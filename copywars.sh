#!/bin/sh
mkdir -p webapps
PATH_DEV="/Users/andre.tan/dev/git/java-forcare-components"
VERSION="1902.1"
cp "$PATH_DEV/projects/index/index/build/libs/index-$VERSION.war" ./webapps/index.war
cp "$PATH_DEV/projects/store/store/build/libs/store-$VERSION.war" ./webapps/store.war
cp "$PATH_DEV/projects/viewer/viewer/build/libs/viewer-$VERSION.war" ./webapps/viewer.war
cp "$PATH_DEV/projects/admin/admin/build/libs/admin-$VERSION.war" ./webapps/admin.war
cp "$PATH_DEV/projects/audit/audit/build/libs/audit-$VERSION.war" ./webapps/admin.war
cp "$PATH_DEV/projects/connect/connect/build/libs/connect-$VERSION.war" ./webapps/connect.war
cp "$PATH_DEV/projects/xca/xca/build/libs/xca-$VERSION.war" ./webapps/xca.war


